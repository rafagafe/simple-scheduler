
//------------------------------------------------------------------------------
/** @file timer-config.h
  * @brief Constant with which the timer module is configured.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------

#ifndef _TIMER_CONFIG_
#define _TIMER_CONFIG_

//------------------------------------------------------------------------------
/** @brief Hertz frequency with which the counter is incremented.           */
#define TIMER_FREQ      100 // Hz

//------------------------------------------------------------------------------
/** @brief  Calculate how many timer ticks are needed to cover a given time.
  *         The minimum amount of tics is one.
  *  @param t: Time in seconds.                                             */
#define __sec(t) (uint32_t)( t * TIMER_FREQ ) ? (uint32_t)( t * TIMER_FREQ ) : 1

//------------------------------------------------------------------------------
/** @brief  Calculate how many timer ticks are needed to cover a given time.
  *         The minimum amount of tics is one.
  *  @param t: Time in miliseconds.                                         */
#define __mSec(t) __sec( t / 1000.0 )

//------------------------------------------------------------------------------
#endif
