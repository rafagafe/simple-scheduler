
//------------------------------------------------------------------------------
/** @file main.c
  * @brief Defines the entry point for the application.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------

//----- Entry:
#include "timer-config.h"
#include "../scheduler/scheduler.h"

//----- Drivers:
#include "../drivers/hardware-profile/hardware-abstraction-layer.h"
#include "../drivers/timer/timer.h"

//----- Data Objects:
#include "../data-objects/all-data-objects.h"

//----- Tasks:
#include "../tasks/uart/uart-tasks.h"
#include "../tasks/timed-text/timed-text-tasks.h"
#include "../tasks/leds-switches/leds-switches-tasks.h"
#include "../tasks/periodic-end/periodic-end-tasks.h"

/** @brief  Elements quantity of the array timedTextContext.                */
#define _TIMED_TEXT_QTY (sizeof(timedTextContext)/sizeof(t_timedTextContext))

/** @brief Array that stores different contexts of a reentrant thread.      */
t_timedTextContext timedTextContext[] = {
    { " Hello every  2.4 seconds.\n", __sec(2.4)        },
    { " Hello every  9.1 seconds.\n", __sec(9.1)        },
    { " Hello every 17.5 seconds.\n", __sec(17.5)       },
    { " Hello every  1.0 minute.\n",  __sec(60.0)       },
    { " Hello every  1.2 minutes.\n", __sec(60.0*1.2)   },
};

//------------------------------------------------------------------------------
/** @brief Application.                                                     */
int main( void ) {

    // Basic hardware configuration:
    hal_cpu_init();
    
    // Initializes data:
    allDataObj_init();
    
    // Sets the timer module to measure the pass of time:
    timer_init( TIMER_FREQ );
    
    // Tasks creation section:
    scheduler_createNewTask( uart_receiveTask );
    scheduler_createNewTask( uart_transmitTask );
    scheduler_createNewTask( uart_comunicationTask );
    scheduler_createNewTask( ledSwitches_task );
    scheduler_createNewTask( periodicEnd_task );

    // Creates multiple reentrant tasks:
    int i; for( i = 0; i < _TIMED_TEXT_QTY; i++ ) {
        int *taskID = scheduler_createNewTask( timedText_task );
        scheduler_setContext( taskID, &timedTextContext[i] );
    }

    // While always:  
    while( true ) {

        // Executes all tasks until all of them get blocked:
        scheduler_run();

        // Enters idle mode. The LED is turned off before entering...
        //      ...and turned on when exits to show the CPU activity:
        hal_ledOff( 6 );
        hal_cpu_idle();
        hal_ledOn( 6 );

    }
    
    // End of application:
    return 0;
}

//------------------------------------------------------------------------------