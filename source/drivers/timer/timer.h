
//------------------------------------------------------------------------------
/** @file timer.c
  * @brief Module that controls a hardware timer so that tasks
  *        can make measurements of time.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------


#ifndef _TIMER_
#define _TIMER_
//------------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>

//------------------------------------------------------------------------------
/** @brief Initializes this module and configure a hardware timer.
  * @param freq: Hertz frequency with which the counter is incremented.
  * This frequency defines the precision with which time is measured.       */
void timer_init( uint16_t freq );

//------------------------------------------------------------------------------
/** @brief Get current timestamp.
  * @return The timestamp in number of timer ticks. @see timer_init().      */
uint32_t timer_getTimeStamp( void );

//------------------------------------------------------------------------------
/** @brief Checks if time has expired.
  * @param timeStamp: Timer tick from when began timing.
  * @param time: Amount of time (in timer ticks) with which it is compared.
  * @return True if time has expired, false otherwise.                      */
bool timer_isTimeOver( uint32_t timeStamp, uint32_t time );

//------------------------------------------------------------------------------
/** @brief Checks if period has expired.
  * @param timeStamp:   When the period began. if period has
  *                     expired it is increased by period.
  * @param period:      Length of period in timer ticks.
  * @return True if time has expired, false otherwise.                      */
bool timer_isPeriodOver( uint32_t *timeStamp, uint32_t period );

//------------------------------------------------------------------------------
#endif