
//------------------------------------------------------------------------------
/** @file timer.c
  * @brief Module that controls a hardware timer so that tasks
  *        can make measurements of time.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------

#include "timer.h"
#include "../hardware-profile/hardware-abstraction-layer.h"

//------------------------------------------------------------------------------
/** @brief Counter of timer tick.                                           */
static volatile uint32_t _ticks = (uint32_t)0;

//------------------------------------------------------------------------------
/** @brief  Callback function invoked by the timer to increase the tick
  *         counter:                                                        */
void _sysTick( void ) { _ticks++; }

//------------------------------------------------------------------------------
// Initializes this module and configures a hardware timer:
void timer_init( uint16_t freq ) {
    hal_sysTimer_config( freq );
    hal_sysTimer_setISR( _sysTick );
}

//------------------------------------------------------------------------------
// Get current timestamp:
uint32_t timer_getTimeStamp( void ) { return _ticks; }

//------------------------------------------------------------------------------
// Checks if time has expired.
bool timer_isTimeOver( uint32_t timeStamp, uint32_t time ) {
    uint32_t currentTick = timer_getTimeStamp();
    uint32_t diference = currentTick - timeStamp;
    if ( currentTick < timeStamp ) diference += UINT32_MAX;
    return (bool)( diference > time  );
}

//------------------------------------------------------------------------------
// Checks if time has period.
bool timer_isPeriodOver( uint32_t *timeStamp, uint32_t period ) {
    if ( timer_isTimeOver( *timeStamp, period ) ) {
        *timeStamp += period;
        return true;
    }
    return false;
}

//------------------------------------------------------------------------------
     