
//------------------------------------------------------------------------------
/** @file hardware-abstraction-layer.c
  * @brief Defines the interface to the hardware. MCU and board.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------

#include "hardware-abstraction-layer.h"
#include "configuration-bits.h"

#define _SET_AS_INPUT   1
#define _SET_AS_OUTPUT  !_SET_AS_INPUT

#define _LED_ON         1
#define _LED_OFF        !_LED_ON

#define _SW_PRESSED     0
#define _SW_RELEASED    !_SW_PRESSED

#define _led3           _LATA0
#define _led4           _LATA1
#define _led5           _LATA2
#define _led6           _LATA3
#define	_led7           _LATA4
#define _led8           _LATA5
#define _led9           _LATA6
#define _led10          _LATA7

#define _led3_tris      _TRISA0
#define _led4_tris      _TRISA1
#define _led5_tris      _TRISA2
#define _led6_tris      _TRISA3
#define	_led7_tris      _TRISA4
#define _led8_tris      _TRISA5
#define _led9_tris      _TRISA6
#define _led10_tris     _TRISA7

#define _sw4            _RD13
#define _sw5            _RA7
#define _sw6            _RD7
#define _sw3            _RD6

#define _sw4_tris       _TRISD13
#define _sw5_tris       _TRISA7
#define _sw6_tris       _TRISD7
#define _sw3_tris       _TRISD6


//------------------------------------------------------------------------------
//------------------------------------------------------------ cpu interface ---
//------------------------------------------------------------------------------
/** @brief Controls the depth of nested critical sections.                  */
static uint16_t _cpuMutex = 0;

//------------------------------------------------------------------------------
// Enter critical section:
void hal_cpu_inputMutex( void ) {
    __builtin_disi( 0x3FFF );
    _cpuMutex++;
}

//------------------------------------------------------------------------------
// Exit critical section:
void hal_cpu_outputMutex( void ) {
    if ( _cpuMutex ) _cpuMutex--;
    if ( !_cpuMutex ) __builtin_disi( 0 );
}

//------------------------------------------------------------------------------
// Initializes the CPU.
void hal_cpu_init( void ) {
    _NSTDIS = 1;
    _cpuMutex = 0;
}

//------------------------------------------------------------------------------
// Set the CPU in idle mode:
void hal_cpu_idle( void ) {   Idle(); }

//------------------------------------------------------------------------------
// Reset:
void hal_cpu_reset( void )  { __asm__ volatile ("reset"); }

//------------------------------------------------------------------------------
/** @brief Trap.                                                            */
void __attribute__((interrupt, no_auto_psv)) _OscillatorFail(void) {
    _OSCFAIL = 0;
    while( true );
}

//------------------------------------------------------------------------------
/** @brief Trap.                                                            */
void __attribute__((interrupt, no_auto_psv)) _AddressError(void) {
    _ADDRERR = 0;    
    while( true );
}

//------------------------------------------------------------------------------
/** @brief Trap.                                                            */
void __attribute__((interrupt, no_auto_psv)) _StackError(void) {
    _STKERR = 0;
    while( true );
}

//------------------------------------------------------------------------------
/** @brief Trap.                                                            */
void __attribute__((interrupt, no_auto_psv)) _MathError(void) { _MATHERR = 0; }

//------------------------------------------------------------------------------
//---------------------------------------------------------------- timer sys ---
//------------------------------------------------------------------------------
/** @brief Pointer to timer callback function.                             */
static callBack_t _sysTimer_tickFunction = 0;

//------------------------------------------------------------------------------
// Set the system timer for it can interrupt periodically.
void hal_sysTimer_config( uint16_t freq ) {
  hal_sysTimer_IRQ( false );
  _sysTimer_tickFunction = 0;
  _T1IP = 6;
  PR1 = ( (uint16_t)( (FCY/256.0) / freq ) );
  T1CONbits.TCKPS = 3;
  T1CONbits.TON = 1;
} 

//------------------------------------------------------------------------------
// Enables or disables the timer interrupt:
void hal_sysTimer_IRQ( bool enable ) { _T1IE = enable; }

//------------------------------------------------------------------------------
// Enables the timer interrupt and install its function:
void hal_sysTimer_setISR( callBack_t function ) {
  _sysTimer_tickFunction = function;
  hal_sysTimer_IRQ( (bool)function );
}

//------------------------------------------------------------------------------
/** @brief ISR of timer.                                                    */
void __attribute__((interrupt, auto_psv)) _T1Interrupt( void ) {
  _T1IF = 0;
  if ( _sysTimer_tickFunction ) _sysTimer_tickFunction();
}

//------------------------------------------------------------------------------
//--------------------------------------------------------------------- uart ---
//------------------------------------------------------------------------------
/** @brief Pointer to reception callback function:                          */
static callBack_t _uart_rxFuntion = 0;

//------------------------------------------------------------------------------
/** @brief Pointer to transmision callback function.                        */
static callBack_t _uart_txFuntion = 0;

//------------------------------------------------------------------------------
// Sets the UART.
void hal_uart_config( void ) {
    hal_uart_rxIRQ( false );
    hal_uart_txIRQ( false );
    U2RXREG; U2RXREG; U2RXREG; U2RXREG;
    _U2TXIF = _U2RXIF = 0;
    _uart_txFuntion = _uart_rxFuntion = 0;
    U2BRG = ( (uint16_t)( FCY / ( 16.0 * 57600 ) ) - 1 );
    U2STA = 0;
    U2MODE = 0x8000;
    U2STAbits.UTXISEL0 = 0;
    U2STAbits.UTXISEL1 = 1;
    U2STAbits.UTXEN = 1;
}

//------------------------------------------------------------------------------
// Gets character received from the UART, if any.
bool hal_uart_getChar( uint8_t *ch ) {
    if ( U2STAbits.URXDA ) {
        *ch = U2RXREG;       
        return true;
    }
    return false;     
}

//------------------------------------------------------------------------------
// Sends a character by the UART, if can:
bool hal_uart_putChar( uint8_t ch ) {
    if ( U2STAbits.UTXBF ) return false;
    U2TXREG = ch;
    return true;
}

//------------------------------------------------------------------------------
// Installs the interrupt routine reception.
void hal_uart_setRxISR( callBack_t rxISR ) {
    hal_uart_rxIRQ( false );
    _U2RXIF = 0;
    _uart_rxFuntion = rxISR;
    hal_uart_rxIRQ( (bool)rxISR );
}

//------------------------------------------------------------------------------
// Installs the interrupt routine transmission.
void hal_uart_setTxISR( callBack_t txISR ) {
    hal_uart_txIRQ( false );
    _U2TXIF = 0;
    _uart_txFuntion = txISR;
    hal_uart_txIRQ( (bool)txISR );
}

//------------------------------------------------------------------------------
// Enables or disables the recption interrupt:
void hal_uart_rxIRQ( bool enable )  { _U2RXIE = enable; }

//------------------------------------------------------------------------------
//Enables or disables the transmision interrupt.:
void hal_uart_txIRQ( bool enable ) { _U2TXIE = enable; }

//------------------------------------------------------------------------------
/** @brief ISR of reception.                                                */
void __attribute__((interrupt, auto_psv)) _U2RXInterrupt( void ) {
    _U2RXIF = 0;
    U2STAbits.PERR = U2STAbits.FERR = U2STAbits.OERR = 0;
    if ( _uart_rxFuntion ) _uart_rxFuntion();
}

//------------------------------------------------------------------------------
/** @brief ISR of transmision.                                              */
void __attribute__((interrupt, auto_psv)) _U2TXInterrupt( void ) {
    _U2TXIF = 0;
    if ( _uart_rxFuntion ) _uart_rxFuntion();
}

//------------------------------------------------------------------------------
//--------------------------------------------------------------------- LEDS ---
//------------------------------------------------------------------------------
// Configures the ports for the LEDs:
void hal_leds_config( void ) {
    _led3 = _led4 = _led5 = _led6  = _LED_OFF;
    _led7 = _led8 = _led9 = _led10 = _LED_OFF;
    _led3_tris = _led4_tris = _led5_tris = _led6_tris = _SET_AS_OUTPUT;
    _led7_tris =_led8_tris = _led9_tris = _led10_tris = _SET_AS_OUTPUT;
}

//------------------------------------------------------------------------------
// Turns on a LED.
void hal_ledOn( uint8_t ledIndex ) {
    switch( ledIndex ) {
        case 0: _led3  = _LED_ON; break;
        case 1: _led4  = _LED_ON; break;
        case 2: _led5  = _LED_ON; break;
        case 3: _led6  = _LED_ON; break;
        case 4: _led7  = _LED_ON; break;
        case 5: _led8  = _LED_ON; break;
        case 6: _led9  = _LED_ON; break;
        case 7: _led10 = _LED_ON; break;
    }
}

//------------------------------------------------------------------------------
// Turns off a LED.
void hal_ledOff( uint8_t ledIndex ) {
    switch( ledIndex ) {
        case 0: _led3  = _LED_OFF; break;
        case 1: _led4  = _LED_OFF; break;
        case 2: _led5  = _LED_OFF; break;
        case 3: _led6  = _LED_OFF; break;
        case 4: _led7  = _LED_OFF; break;
        case 5: _led8  = _LED_OFF; break;
        case 6: _led9  = _LED_OFF; break;
        case 7: _led10 = _LED_OFF; break;
    }
}

//------------------------------------------------------------------------------
// Changes the state of a LED.
void hal_ledToggle( uint8_t ledIndex ) {
    switch( ledIndex ) {
        case 0: _led3  = !_led3;  break;
        case 1: _led4  = !_led4;  break;
        case 2: _led5  = !_led5;  break;
        case 3: _led6  = !_led6;  break;
        case 4: _led7  = !_led7;  break;
        case 5: _led8  = !_led8;  break;
        case 6: _led9  = !_led9;  break;
        case 7: _led10 = !_led10; break;
    }
}

//------------------------------------------------------------------------------
//----------------------------------------------------------------- switches ---
//------------------------------------------------------------------------------
// Configures the ports for the switches:
void hal_switches_config( void ) {
    _sw4_tris = _sw5_tris = _sw6_tris = _sw3_tris = _SET_AS_INPUT;
}

//------------------------------------------------------------------------------
//Checks if a switch is pressed:
bool hal_switch_isPressed( uint8_t switchIndex ) {
    bool retVal;
    switch( switchIndex ) {
        case 0:  retVal = ( _sw4 == _SW_PRESSED );  break;
        case 1:  retVal = ( _sw5 == _SW_PRESSED );  break;
        case 2:  retVal = ( _sw6 == _SW_PRESSED );  break;
        case 3:  retVal = ( _sw3 == _SW_PRESSED );  break;
        default: retVal = false;
    }
    return retVal;
}

//------------------------------------------------------------------------------
//Checks if a switch is released:
bool hal_switch_isReleased( uint8_t switchIndex ) {
    bool retVal;
    switch( switchIndex ) {
        case 0:  retVal = ( _sw4 == _SW_RELEASED );  break;
        case 1:  retVal = ( _sw5 == _SW_RELEASED );  break;
        case 2:  retVal = ( _sw6 == _SW_RELEASED );  break;
        case 3:  retVal = ( _sw3 == _SW_RELEASED );  break;
        default: retVal = true;
    }
    return retVal;
}

//------------------------------------------------------------------------------
