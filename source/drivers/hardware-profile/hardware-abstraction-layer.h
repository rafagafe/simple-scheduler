

//------------------------------------------------------------------------------
/** @file hardware-abstraction-layer.h
  * @brief Declares the interface to the hardware. MCU and board.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------


#ifndef _HARD_
#define _HARD_
//------------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>

//------------------------------------------------------------------------------
/** @brief Type pointer to function for use as callbacks.                   */
typedef void (*callBack_t)( void );

//------------------------------------------------------------------------------
/** @brief Instruction frequency.                                           */
#define FCY                 ((uint32_t)16000000)

//------------------------------------------------------------------------------
/** @brief LEDS quantity on board.                                          */
#define HARD_LEDS_QTY       7

//------------------------------------------------------------------------------
/** @brief Switches quantity on board.                                      */
#define HARD_SWITCH_QTY     4

//------------------------------------------------------------------------------
//------------------------------------------------------------ cpu interface ---
//------------------------------------------------------------------------------
/** @brief Initializes the CPU.                                             */
void hal_cpu_init( void );

//------------------------------------------------------------------------------
/** @brief Set the CPU in idle mode.                                         */
void hal_cpu_idle( void );

//------------------------------------------------------------------------------
/** @brief Reset.                                                           */
void hal_cpu_reset( void );

//------------------------------------------------------------------------------
/** @brief Enter critical section.                                         */
void hal_cpu_inputMutex( void );

//------------------------------------------------------------------------------
/** @brief Exit critical section.                                           */
void hal_cpu_outputMutex( void );

//------------------------------------------------------------------------------
//---------------------------------------------------------------- timer sys ---
//------------------------------------------------------------------------------
/** @brief Sets the system timer for it can interrupt periodically.
  * @param freq: Frecuencia del Tic                                         */
void hal_sysTimer_config( uint16_t freq );

//------------------------------------------------------------------------------
/** @brief Enables the timer interrupt and install its function.
  * @param function: Callback function pointer.                             */
void hal_sysTimer_setISR( callBack_t function );

//------------------------------------------------------------------------------
/** @brief Enables or disables the timer interrupt.
  * @param enable: True to enable, false to disable.                         */
void hal_sysTimer_IRQ( bool enable );

//------------------------------------------------------------------------------
//--------------------------------------------------------------------- uart ---
//------------------------------------------------------------------------------
/** @brief Sets the UART. (57600bps).                                       */
void hal_uart_config( void );

//------------------------------------------------------------------------------
/** @brief Gets character received from the UART, if any.
  * @param ch[out]: Destine.
  * @return Returns true if it has got, or false otherwise.                 */
bool hal_uart_getChar( uint8_t *ch );

//------------------------------------------------------------------------------
/** @brief Sends a character by the UART, if can
  * @param ch: Character to send.
  * @return Returns true if it has got, or false otherwise.                 */
bool hal_uart_putChar( uint8_t ch );

//------------------------------------------------------------------------------
/** @brief Installs the interrupt routine reception.
  * @param rxISR: Pointer to function.                                      */
void hal_uart_setRxISR( callBack_t rxISR );

//------------------------------------------------------------------------------
/** @brief Installs the interrupt routine transmission.
  * @param rxISR: Pointer to function.                                      */
void hal_uart_setTxISR( callBack_t txISR );

//------------------------------------------------------------------------------
/** @brief Enables or disables the recption interrupt.
  * @param enable: True to enable, false to disable.                         */
void hal_uart_rxIRQ( bool enable );

//------------------------------------------------------------------------------
/** @brief Enables or disables the transmision interrupt.
  * @param enable: True to enable, false to disable.                         */
void hal_uart_txIRQ( bool enable );

//------------------------------------------------------------------------------
//--------------------------------------------------------------------- LEDS ---
//------------------------------------------------------------------------------
/** @brief Configures the ports for the LEDs.                               */
void hal_leds_config( void );

//------------------------------------------------------------------------------
/** @brief Turns on a LED.
  * @param ledIndex: Indicates the LED to operate.                          */
void hal_ledOn( uint8_t ledIndex );

//------------------------------------------------------------------------------
/** @brief  Turns off a LED.
  * @param ledIndex: Indicates the LED to operate.                          */
void hal_ledOff( uint8_t ledIndex );

//------------------------------------------------------------------------------
/** @brief Changes the state of a LED.
  * @param ledIndex: Indicates the LED to operate.                          */
void hal_ledToggle( uint8_t ledIndex );

//------------------------------------------------------------------------------
//----------------------------------------------------------------- switches ---
//------------------------------------------------------------------------------
/** @brief Configures the ports for the switches.                           */
void hal_switches_config( void );

//------------------------------------------------------------------------------
/** @brief Checks if a switch is pressed.
  * @param switchIndex: Indicates the switch to operate.
  * @return True if it is, false otherwise.                                 */
bool hal_switch_isPressed( uint8_t switchIndex );

//------------------------------------------------------------------------------
/** @brief Checks if a switch is released.
  * @param switchIndex: Indicates the switch to operate.
  * @return True if it is, false otherwise.                                 */
bool hal_switch_isReleased( uint8_t switchIndex );

//------------------------------------------------------------------------------
#endif
