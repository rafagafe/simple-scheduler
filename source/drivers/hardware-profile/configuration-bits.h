
//------------------------------------------------------------------------------
/** @file configuration-bits.h
  *  @brief Defines the configuration bits.
  *  This configuration is for PIC24FJ128GA010 on Explorer16 evaluation board.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------

#ifndef _CONFIGURATION_BITS_
#define	_CONFIGURATION_BITS_
//------------------------------------------------------------------------------

#include <xc.h>

_CONFIG1(  WDTPS_PS1024  & // Watchdog Timer Postscaler: 1:1
           FWPSA_PR32    & // WDT Prescaler: Prescaler ratio of 1:32
           WINDIS_OFF    & // Watchdog Timer Window: Windowed Watchdog Timer enabled; FWDTEN must be 1
           FWDTEN_OFF    & // Watchdog Timer Enable: Watchdog Timer is enabled
           ICS_PGx2      & // Comm Channel Select: Emulator/debugger uses EMUC2/EMUD2
           COE_OFF       & // Set Clip On Emulation Mode: Reset Into Operational Mode
           BKBUG_OFF     & // Background Debug: Device resets into Operational mode
           GWRP_OFF      & // General Code Segment Write Protect: Writes to program memory are allowed
           GCP_OFF       & // General Code Segment Code Protect: Code protection is disabled
           JTAGEN_OFF      // JTAG Port Enable: JTAG port is disabled
        );
        
_CONFIG2( POSCMOD_HS   & // Primary Oscillator Select: HS Oscillator mode selected
          OSCIOFNC_OFF & // Primary Oscillator Output Function: OSC2/CLKO/RC15 functions as port I/O (RC15)
          FNOSC_PRIPLL & // Oscillator Select: Primary Oscillator with PLL module (HSPLL, ECPLL)
          IESO_OFF       // Internal External Switch Over Mode: IESO mode (Two-Speed Start-up) disabled
        );


//------------------------------------------------------------------------------
#endif	/* _CONFIGURATION_BITS_ */

