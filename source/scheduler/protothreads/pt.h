

//------------------------------------------------------------------------------
/** @file pt.h
  * @brief Adam Dunkels threads that have been added one state else to see
  *        if may one enter low power mode.
  * @date 04/01/2014.                                                       */
//------------------------------------------------------------------------------

#ifndef __PT_H__
#define __PT_H__
//------------------------------------------------------------------------------

#include "lc.h"

#define PT_BLOCKED  0 /**< It's waiting the same event than last time.  */
#define PT_WAITING  1 /**< It's waiting a new event from last time.     */
#define PT_YIELDED  2
#define PT_EXITED   3
#define PT_ENDED    4


typedef struct pt { lc_t lc; } pt_t;

#define PT_THREAD(name_args) char name_args

//------------------------------------------------------------------------------
/** @brief Initialize a protothread.
  *
  * Initializes a protothread. Initialization must be done prior to
  * starting to execute the protothread.   
  * @param pt: A pointer to the protothread control structure.              */
#define PT_INIT( pt )     LC_INIT( (pt)->lc )
#define pt_init           PT_INIT

//------------------------------------------------------------------------------
/** @brief Declare the start of a protothread inside the C function
  * implementing the protothread.
  * 
  * This macro is used to declare the starting point of a
  * protothread. It should be placed at the start of the function in
  * which the protothread runs. All C statements above the pt_begin()
  * invokation will be executed each time the protothread is scheduled.    
  * @param pt: A pointer to the protothread control structure.              */
#define PT_BEGIN( pt )    { char _stillWaitingFlag = 1; LC_RESUME( (pt)->lc )

//------------------------------------------------------------------------------
/** @brief Declare the end of a protothread.
  *
  * This macro is used for declaring that a protothread ends. It must
  * always be used together with a matching pt_begin() macro.  
  * @param pt: A pointer to the protothread control structure.              */
#define PT_END( pt )      LC_END((pt)->lc); _stillWaitingFlag = 0;           \
                          PT_INIT(pt); return PT_ENDED; }
                                                    
//------------------------------------------------------------------------------
/** @brief Block and wait until condition is true. 
  *
  * This macro blocks the protothread until the specified condition is true.
  * @param pt: A pointer to the protothread control structure.              
  * @param condition: The condition.                                        */
#define PT_WAIT_UNTIL( pt, condition )                          \
    do {						        \
        _stillWaitingFlag = 0;                                  \
        LC_SET( (pt)->lc ); 		                        \
        if( !(condition) ) {  		                        \
            if ( _stillWaitingFlag ) { return PT_BLOCKED; }     \
            return PT_WAITING;			                \
        }						        \
    } while(0)

//------------------------------------------------------------------------------
/** @brief Block and wait while condition is true. 
  *
  * This macro blocks the protothread while the specified condition is true.
  * @param pt: A pointer to the protothread control structure.              
  * @param condition: The condition.                                        */
#define PT_WAIT_WHILE( pt, cond )  PT_WAIT_UNTIL( (pt), !(cond) )

//------------------------------------------------------------------------------
/** @brief Block and wait while condition is true. 
  *
  * This macro blocks the protothread while the specified condition is true.
  * @param pt: A pointer to the protothread control structure.              
  * @param condition: The condition.                                        */
#define PT_WAIT_THREAD( pt, thread )                            \
    do {						        \
        LC_SET( (pt)->lc ); 		                        \
         _stillWaitingFlag = thread;                            \
        if( _stillWaitingFlag < PT_EXITED ) {                   \
            return _stillWaitingFlag;			        \
        }						        \
    } while(0)

//------------------------------------------------------------------------------
/** @brief Spawn a child protothread and wait until it exits.
  *
  * This macro spawns a child protothread and waits until it exits. The
  * macro can only be used within a protothread.
  * @param pt: A pointer to the protothread control structure.              
  * @param child: A pointer to the child protothread's control structure.              
  * @param thread: The child protothread with arguments.                    */
#define PT_SPAWN( pt, child, thread ) 	                        \
    do {						        \
        PT_INIT( (child) );		                        \
        PT_WAIT_THREAD( (pt), (thread) );                       \
    } while(0)

//------------------------------------------------------------------------------
/** @brief Restart the protothread.
  *
  * This macro will block and cause the running protothread to restart
  * its execution at the place of the pt_begin() call.   
  * @param pt: A pointer to the protothread control structure.              */
#define PT_RESTART( pt )			                \
    do {						        \
        PT_INIT(pt);				                \
        return PT_WAITING;			                \
    } while(0)    
    
//------------------------------------------------------------------------------
/** @brief Exit the protothread.
  *
  * This macro causes the protothread to exit. If the protothread was
  * spawned by another protothread, the parent protothread will become
  * unblocked and can continue to run.  
  * @param pt: A pointer to the protothread control structure.              */
#define PT_EXIT( pt )			                        \
    do {						        \
        PT_INIT( pt );			                        \
        return PT_EXITED;			                \
    } while(0)
#define pt_exit         PT_EXIT

//------------------------------------------------------------------------------
/** @brief Schedule a protothread.
  *
  * This function shedules a protothread. The return value of the
  * function is non-zero if the protothread is running or zero if the
  * protothread has exited.
  * @param f: The call to the C function implementing 
  *                                     the protothread to be scheduled.   */
#define PT_SCHEDULE( f ) ( ( f ) < PT_EXITED )

//------------------------------------------------------------------------------
/** @brief Yield from the current protothread.
  *
  * This function will yield the protothread, thereby allowing other
  * processing to take place in the system.
  * @param pt: A pointer to the protothread control structure.              */
#define PT_YIELD( pt )			                        \
  do {						                \
      _stillWaitingFlag = false;                                \
      LC_SET( (pt)->lc );			                \
      if( !_stillWaitingFlag ) {		  	        \
          return PT_YIELDED;			                \
      }						                \
  } while(0)
#define pt_yield        PT_YIELD
  
//------------------------------------------------------------------------------
/** @brief Yield from the protothread until a condition occurs.
  * @param pt: A pointer to the protothread control structure.              
  * @param condition: The condition.                                        */
#define PT_YIELD_UNTIL( pt, cond )                              \
  do {						                \
      _stillWaitingFlag = false;                                \
      LC_SET((pt)->lc);				                \
      if( !_stillWaitingFlag || !(cond) ) {                     \
          return PT_YIELDED;			                \
      }						                \
  } while(0)

//------------------------------------------------------------------------------
#endif /* __PT_H__ */




