

//------------------------------------------------------------------------------
/** @file pt.h
  * @brief Adam Dunkels threads that have been added one state else to see
  *        if may one enter low power mode.
  * @date 04/01/2014.                                                       */
//------------------------------------------------------------------------------

#ifndef __PT_SEM_H__
#define __PT_SEM_H__
//------------------------------------------------------------------------------

#include "pt.h"

struct pt_sem {
  unsigned int count;
};

//------------------------------------------------------------------------------
/** @brief Initialize a semaphore.
  *
  * This macro initializes a semaphore with a value for the
  * counter. Internally, the semaphores use an "unsigned int" to
  * represent the counter, and therefore the "count" argument should be
  * within range of an unsigned int.  
  * @param s:   (struct pt_sem *) A pointer to the pt_sem struct
  *             representing the semaphore
  * @param c: (unsigned int) The initial count of the semaphore.            */
#define PT_SEM_INIT(s, c) (s)->count = c

//------------------------------------------------------------------------------
/** @brief Wait for a semaphore.
  *
  * This macro carries out the "wait" operation on the semaphore. The
  * wait operation causes the protothread to block while the counter is
  * zero. When the counter reaches a value larger than zero, the
  * protothread will continue. 
  * @param pt: (struct pt *) A pointer to the protothread (struct pt) in
  *            which the operation is executed.
  * @param s: (struct pt_sem *) A pointer to the pt_sem struct
  *            representing the semaphore                                   */ 
#define PT_SEM_WAIT(pt, s)                  \
  do {                                      \
    PT_WAIT_UNTIL(pt, (s)->count > 0);      \
    --(s)->count;                           \
  } while(0)

//------------------------------------------------------------------------------
/** @brief Signal a semaphore.
  *
  * This macro carries out the "signal" operation on the semaphore. The
  * signal operation increments the counter inside the semaphore, which
  * eventually will cause waiting protothreads to continue executing.
  * @param pt: (struct pt *) A pointer to the protothread (struct pt) in
  *            which the operation is executed.
  * @param s: (struct pt_sem *) A pointer to the pt_sem struct
  *            representing the semaphore                                   */ 
#define PT_SEM_SIGNAL(pt, s) ++(s)->count

//------------------------------------------------------------------------------
#endif /* __PT_SEM_H__ */

