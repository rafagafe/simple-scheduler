
//------------------------------------------------------------------------------
/** @file scheduler.c
  * @brief Simple scheduler.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------

#include "scheduler.h"
#include <stdlib.h>

//------------------------------------------------------------------------------
/** @brief Structure to form a ring tasks:                                  */
typedef struct s_task {
    thread_t thread;            /**< Pointer to protothread function.       */
    struct s_task *next;        /**< Points the next task in the ring.      */
    struct pt pt;               /**< Protothread state struct.              */
    void *context;              /**< Pointer to the context of task.        */
} task_t;

//------------------------------------------------------------------------------
/** @brief Points to a task within the ring of tasks.                       */
static task_t* _ring = 0;

//------------------------------------------------------------------------------
/** @brief Indicates how many tasks have the ring.                          */
unsigned int _ring_tasksQty = 0;

//------------------------------------------------------------------------------
/** @brief Inserts a task ring tasks.                                       */
static void _ring_input( task_t *task ) {
  // Increases the processes counter:
  _ring_tasksQty++;
  // Inserts the process:
  if ( !_ring ) _ring = task;
  else task->next = _ring->next;
  _ring->next = task;
} 

//------------------------------------------------------------------------------
/** @brief Get a task on the ring of tasks.                                 */  
static void _ring_output( task_t *task ) {
    // If the ring of tasks is empty does nothing: 
    if ( !_ring ) return;
    task_t *iter = _ring;
    int qty = 0;
    // Search the prior task within the ring:
    while( qty < _ring_tasksQty && iter->next != task ) {
        iter = iter->next;
        qty++;
    }
    // If the prior task has been found, the intended one can be got:
    if ( qty < _ring_tasksQty ) {
        iter->next = task->next;
        task->next = 0;
        _ring_tasksQty--;
    }              
}   

//------------------------------------------------------------------------------
// Creates a task and is put into ring of tasks:
int* scheduler_createNewTask( thread_t thread ) {
    // If no thread does nothing:
    if ( !thread ) return 0;
    // Reserves memory for the task:
    task_t *task = malloc( sizeof(task_t) );
    // If succeeded initializes:
    if ( task ) {
        task->thread     = thread;
        task->context    = 0;
        task->next       = 0;
        PT_INIT( &task->pt );
        // The task is put into the ring of tasks:
        _ring_input( task );
    }
    // Returns de ID:
    return (int*)task;
}

//------------------------------------------------------------------------------
// Kills the task:
static void _kill( task_t *task ) {
    if ( !task ) return;
    _ring_output( task );
    free( task );
}

//------------------------------------------------------------------------------
// Sets the contexot of a previously created task:
void scheduler_setContext( int* id, void* context ) {
    task_t *task = (task_t *)id;
    if ( task && context ) task->context = context;
}

//------------------------------------------------------------------------------
// Executes all tasks until all of them get blocked:  
void scheduler_run( void ) {
    int blockedQty = 0; 
    // While not all tasks are blocked:      
    while( blockedQty < _ring_tasksQty ) {
        task_t *run = _ring;
        _ring = _ring->next;
        char st = run->thread( &run->pt, run->context );
        // If blocked:
        if ( st == PT_BLOCKED )
            // Increases the consecutives blorcked task counter: 
            blockedQty++;
        // If not blocked:    
        else {
            // Resets the consecutives blocked task counter: 
            blockedQty = 0;           
            // If the thread was finished must be destroyed:
            if ( st >= PT_EXITED ) _kill( run );
        } 
    }  
}

//------------------------------------------------------------------------------



  
  
  

