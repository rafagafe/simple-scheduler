
//------------------------------------------------------------------------------
/** @file scheduler.h
  * @brief Simple scheduler.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------


#ifndef _SCHEDULER_
#define _SCHEDULER_
//------------------------------------------------------------------------------

#include "protothreads/pt.h"
#include "protothreads/pt-sem.h"

//------------------------------------------------------------------------------
/** @brief Defines the pointer to protothread function.
  * When the scheduler invokes the protothread passes the following arguments: 
  *  (1) Pointer to the protothread structure.  
  *  (2) Pointer to the context.                                            */
typedef PT_THREAD( (*thread_t)( struct pt*, void* )  );

//------------------------------------------------------------------------------
/** @brief Creates a task from protothread. 
  * @param thread: Pointer to the protothread function.
  * @return The task identifier.                                            */
int* scheduler_createNewTask( thread_t thread );

//------------------------------------------------------------------------------
/** @brief Executes all active tasks until all get blocked.                 */ 
void scheduler_run( void );

//------------------------------------------------------------------------------
/** @brief Sets the contexot of a previously created task.
  * @param id: The task identifier.                                         
  * @param context: Pointer to context.                                     */
void scheduler_setContext( int* id, void* context );

//------------------------------------------------------------------------------
#endif
    
