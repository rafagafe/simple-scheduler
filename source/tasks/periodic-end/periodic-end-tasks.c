
//------------------------------------------------------------------------------
/** @file periodic-end-tasks.c
  * @brief In this file the task that make an LED blink is defined.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------

//----- Entry:
#include "../../entry/timer-config.h"
#include "../../scheduler/scheduler.h"

//----- Drivers:
#include "../../drivers/hardware-profile/hardware-abstraction-layer.h"
#include "../../drivers/timer/timer.h"

//----- Data Objects:
#include "../../data-objects/all-data-objects.h"

//----- Tasks:
#include "periodic-end-tasks.h"

#define _PERIOD     0.2 // Seconds

//------------------------------------------------------------------------------
// This task flashes an LED. This waits 0.2 seconds...
//                  ...to change the LED status, create a clone task and die:
PT_THREAD( periodicEnd_task( struct pt *pt, void *context ) ) {
    static uint32_t time = (uint32_t)0;
    PT_BEGIN( pt );
    PT_WAIT_UNTIL( pt, timer_isPeriodOver( &time, __sec(_PERIOD) ) );
    hal_ledToggle( 4 );
    scheduler_createNewTask( periodicEnd_task );
    PT_END( pt );
}

//------------------------------------------------------------------------------
