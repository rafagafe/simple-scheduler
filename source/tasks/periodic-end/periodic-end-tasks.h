
//------------------------------------------------------------------------------
/** @file periodic-end-tasks.h
  * @brief In this file the task that make an LED blink is declared
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------


#ifndef _PERIODIC_END_
#define _PERIODIC_END_
//------------------------------------------------------------------------------

#include "../../scheduler/scheduler.h"

//------------------------------------------------------------------------------
/** @brief This task flashes an LED. This waits 0.2 seconds to change
  *        the LED status, create a clone task and die.                     */
PT_THREAD( periodicEnd_task( struct pt *pt, void *context ) );


//------------------------------------------------------------------------------
#endif
