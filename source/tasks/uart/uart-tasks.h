
//------------------------------------------------------------------------------
/** @file uart-tasks.h
  * @brief In this file the UART control tasks are declared.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------


#ifndef _UART_
#define _UART_
//------------------------------------------------------------------------------

#include "../../scheduler/scheduler.h"

//------------------------------------------------------------------------------
/** @brief This task gets the bytes received by the
  *        UART and places them in a queue.                                 */
PT_THREAD( uart_receiveTask( struct pt *pt, void *context ) );

//------------------------------------------------------------------------------
/** @brief This task gets the bytes of the output queue (declared
  *        in the "all-data-objects" module) and sends the UART.            */
PT_THREAD( uart_transmitTask( struct pt *pt, void *context ) ) ;

//------------------------------------------------------------------------------
/** @brief This task gets the bytes from the input queue (used by
  *        uart_receiveTask ()) and places them in the output queue
  *        (declared in the "all-data-objects" module). Access to the 
  *        output queue is controlled by a semaphore ( declared in
  *        the "all-data-objects" module). If the character received
  *        is between 1 and 4 then activates an LED flag ( defined
  *        in "all-data-objects" and used by ledSwitches_task() ).          */
PT_THREAD( uart_comunicationTask( struct pt *pt, void *context ) );

//------------------------------------------------------------------------------
#endif
