
//------------------------------------------------------------------------------
/** @file uart-tasks.c
  * @brief In this file the UART control tasks are defined.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------

//----- Drivers:
#include "../../drivers/hardware-profile/hardware-abstraction-layer.h"

//----- Data Objects:
#include "../../data-objects/all-data-objects.h"

//----- Tasks:
#include "uart-tasks.h"
 
//------------------------------------------------------------------------------
/** @brief Input queue length.                                              */
#define _INPUT_LENGTH        1

//------------------------------------------------------------------------------
/** @brief Memory for the input queue.                                      */
static uint8_t inputData[ _INPUT_LENGTH ];

//------------------------------------------------------------------------------
/** @brief Queue for data received from the UART.                           */                                                 
static fifo_t _input = { 0, 0, 0, _INPUT_LENGTH, inputData };

//------------------------------------------------------------------------------
// This task gets the bytes received by the UART and places them in a queue:
PT_THREAD( uart_receiveTask( struct pt *pt, void *context ) ) {
    // For bytes transfers:
    static uint8_t ch;
    
    // Beginning protothread:
    PT_BEGIN( pt );
    
    // Sets the serial port:
    hal_uart_config();
    // The interrupts are enabled, we don't need handle these...
    //          ...events but we need that they wake to the CPU from idle mode:
    hal_uart_rxIRQ( true );
    hal_uart_txIRQ( true );
    // Empty queues:
    fifo_flush( &_input );      
    fifo_flush( &output );
    // Put in the output queue for an initial message, to the fit:
    char *str = "\n\n\t\t�Reset!\n\n";
    while( *str && fifo_put( &output, *(str++) ) ); 
         
    // While always:
    while( true ) {
        // Waits for a character:
        PT_WAIT_UNTIL( pt, hal_uart_getChar( &ch ) );
        // Waits until puts the received character into the input fifo:
        PT_WAIT_UNTIL( pt, fifo_put( &_input, ch ) );
    }
    
    // End of protothread:
    PT_END( pt );
}

//------------------------------------------------------------------------------
// This task gets the bytes of the output queue (declared in the...
//                          ..."all-data-objects" module) and sends the UART:
PT_THREAD( uart_transmitTask( struct pt *pt, void *context ) ) {
    // Variable to transfer characters:
    static uint8_t ch;
    
    // Beginning protothread:
    PT_BEGIN( pt );
       
    // While always:
    while( true ) {
        // Waits until there is data in the output queue:
        PT_WAIT_UNTIL( pt, fifo_get( &output, &ch ) );
        // Waits until you can send by uart.
        PT_WAIT_UNTIL( pt, hal_uart_putChar( ch ) );
    }
    
    // End of protothread:
    PT_END( pt );
}

//------------------------------------------------------------------------------
// This task gets the bytes from the input queue ( used by uart_receiveTask() )
// and places them in the output queue ( declared in the "all-data-objects"
// module ). Access to the output queue is controlled by a semaphore ( declared 
// in the "all-data-objects" module). If the character received is  between
// 1 and 4 then activates an LED flag ( defined in "all-data-objects"  and used
// by ledSwitches_task() ):
PT_THREAD( uart_comunicationTask( struct pt *pt, void *context ) ) {
    // Variable to transfer characters:
    static uint8_t ch;
    
    // Beginning protothread:
    PT_BEGIN( pt );
    
    // While always:
    while( true ) {
        // Waits until there is data in the input queue:
        PT_WAIT_UNTIL( pt, fifo_get( &_input, &ch ) );
        // If the character indicates an LED. This is activated:
        if ( ch >= '1' && ch < DATA_OBJECT_LEDS_QTY + '1')
            leds[ ch-'1' ] = true;
        // Waits until the output queue is free:
        PT_SEM_WAIT( pt, &sem );
        // Waits until put it in the output queue:
        PT_WAIT_UNTIL( pt, fifo_put( &output, ch ) );
        // Frees the output queue:
        PT_SEM_SIGNAL( pt, &sem );
    }
    
    // End of protothread:
    PT_END( pt );
}
//------------------------------------------------------------------------------
