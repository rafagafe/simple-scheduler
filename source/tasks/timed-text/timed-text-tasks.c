
//------------------------------------------------------------------------------
/** @file timed-text-tasks.c
  * @brief
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------

//----- Entry:
#include "../../entry/timer-config.h"

//----- Drivers:
#include "../../drivers/timer/timer.h"

//----- Data Objects:
#include "../../data-objects/all-data-objects.h"

//----- Tasks:
#include "timed-text-tasks.h"


//------------------------------------------------------------------------------
// This reentrant task periodically writes a text with a
// period defined both in context. The text is written to
// the output queue. Access to the output queue is controlled
// by a semaphore (both declared in the "all-data-objects" module):
PT_THREAD( timedText_task( struct pt *pt, void *context ) ) {
    // Casts the context:
    t_timedTextContext *c = (t_timedTextContext *)context;
    
    // Beginning of the protothread:
    PT_BEGIN( pt );
    
    // Capture the moment in which the loop starts:
    c->time = timer_getTimeStamp();
    
    // While always:
    while( true ) {
        // Waits until the expiration of this period:
        PT_WAIT_UNTIL( pt, timer_isPeriodOver( &c->time, c->period ) );
        // Waits until the output queue is free:
        PT_SEM_WAIT( pt, &sem );
        // It points to the start of the text:
        c->index = 0;
        // Executes until finding the null character:
        while( c->text[c->index] ) {
            // Waits to put the character in the output queue:
            PT_WAIT_UNTIL( pt, fifo_put( &output, c->text[c->index]) );
            // It points to the next character:
            c->index++;
        }        
        // Frees the output queue:
        PT_SEM_SIGNAL( pt, &sem );
    }
    
    // End of protothread:
    PT_END( pt );
}


//------------------------------------------------------------------------------

