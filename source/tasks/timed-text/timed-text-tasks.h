
//------------------------------------------------------------------------------
/** @file timed-text-tasks.h
  * @brief    
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------


#ifndef _TIMED_TEXT_
#define _TIMED_TEXT_
//------------------------------------------------------------------------------

#include "../../scheduler/scheduler.h"

//------------------------------------------------------------------------------
/** @brief Type of data to store the context of the TIMED-TEXT protothread. */
typedef struct s_timedTextContext {
    // Need to be initialized:
    const char *text;   /**< Pointer to the text displayed.             */
    uint32_t period;    /**< Indicates its thread the working period.   */
    // No need to be initialized:
    uint32_t time;      /**< Used to measure the passage of time.       */
    uint8_t index;      /**< Iterator.                                  */
} t_timedTextContext;


//------------------------------------------------------------------------------
/** @brief This reentrant task periodically writes a text with a period
  *        defined both in context. The text is written to the output queue.
  *        Access to the output queue is controlled by a semaphore ( both
  *        declared in the "all-data-objects" module).                      */
PT_THREAD( timedText_task( struct pt *pt, void *context ) );

//------------------------------------------------------------------------------
#endif
