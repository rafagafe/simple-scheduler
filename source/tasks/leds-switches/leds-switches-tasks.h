
//------------------------------------------------------------------------------
/** @file leds-switches-tasks.h
  * @brief This file tasks that control buttons and LEDs are declared.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------


#ifndef _LEDS_SWITCHES_
#define _LEDS_SWITCHES_
//------------------------------------------------------------------------------

#include "../../scheduler/scheduler.h"

//------------------------------------------------------------------------------
/** @brief This task scans every 100mS buttons. If any button is pressed its 
  *        LED turns on for half a second. This task creates a task for each 
  *        LED also. When a pressed button is detected this task activates one 
  *        flag that is defined in "all-objects-data".  The tasks of the LED 
  *        expect their flags to turn on its LED for half a second.         */
PT_THREAD( ledSwitches_task( struct pt *pt, void *context ) );

//------------------------------------------------------------------------------
#endif
