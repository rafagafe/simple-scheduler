
//------------------------------------------------------------------------------
/** @file leds-switches-tasks.c
  * @brief This file tasks that control buttons and LEDs are defined.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------

//----- Entry:
#include "../../entry/timer-config.h"
#include "../../scheduler/scheduler.h"

//----- Drivers:
#include "../../drivers/hardware-profile/hardware-abstraction-layer.h"
#include "../../drivers/timer/timer.h"

//----- Data Objects:
#include "../../data-objects/all-data-objects.h"

//----- Tasks:
#include "leds-switches-tasks.h"


//------------------------------------------------------------------------------
/** @brief Contexts for led delay tasks:                                    */
struct s_ledContext {
  uint8_t ledIndex;     /**< Index of controlled LED.   */
  uint32_t time;        /**< Remenber a timestamp.      */
} _ledContext[HARD_SWITCH_QTY];


//------------------------------------------------------------------------------
/** @brief Reentrant task before a flag indicated in context to turn on an
  *        LED for half a second to activate.                               */
PT_THREAD( _ledDelayTask( struct pt *pt, void *context ) ) {
    // Casts the context:
    struct s_ledContext *c = (struct s_ledContext *)context;

    // Beginning of the protothread:
    PT_BEGIN( pt );

    // While always:
    while( true ) {
        // Waits until the flag of its LED is activated:
        PT_WAIT_UNTIL( pt, leds[c->ledIndex] );
        // Captures the moment:
        c->time =timer_getTimeStamp();
        // Disables the flag:
        leds[c->ledIndex] = false;
        // Turns on its LED:
        hal_ledOn( c->ledIndex );
        // Wait half a second:
        PT_WAIT_UNTIL( pt, timer_isTimeOver( c->time, __sec(0.5) ) );
        // Turns off its LED:
        hal_ledOff( c->ledIndex );
    }

    // End of protothread:
    PT_END( pt );
}

//------------------------------------------------------------------------------
// This task scans every 100mS buttons. If any button...
//                      ...is pressed its LED turns on for half a second:
PT_THREAD( ledSwitches_task( struct pt *pt, void *context ) ) {
    uint8_t sw;     // Iterador.
    int *taskID;    // Task identifier
    static uint32_t time;
    
    // Beginning of the protothread:
    PT_BEGIN( pt );

    // Hardware configuration:
    hal_leds_config();
    hal_switches_config();

    // A protothread is created for each LED:
    for( sw = 0; sw < HARD_SWITCH_QTY; sw++ ) {
        _ledContext[sw].ledIndex = sw;
        taskID = scheduler_createNewTask( _ledDelayTask );
        scheduler_setContext( taskID, &_ledContext[sw] );
    }    
    
    // Capture the moment in which the loop starts:
    time = timer_getTimeStamp();    
    
    // While always:
    while( true ) {
        // Waits until the expiration of this period (100ms-10Hz):
        PT_WAIT_UNTIL( pt, timer_isPeriodOver( &time, __sec(0.1) ) );
        // It iterates through each button:
        for( sw = 0; sw < HARD_SWITCH_QTY; sw++ )
            // If pressed sets the flag of the associated LED:
            if ( hal_switch_isPressed( sw ) ) leds[sw] = true;
    }       
    
    // End of protothread:
    PT_END( pt );
}

//------------------------------------------------------------------------------
