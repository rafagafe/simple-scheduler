
//------------------------------------------------------------------------------
/** @file all-data-objects.h
  * @brief Defines the objects  of data visible between tasks
  *         in different modules.
  * @date 04/01/2014
  * @author Rafa Garc�a                                                     */
//------------------------------------------------------------------------------


#ifndef _DATA_OBJECTS_
#define _DATA_OBJECTS_
//------------------------------------------------------------------------------

#include "../scheduler/scheduler.h"
#include "../libraries/fifo/fifo.h"

#define DATA_OBJECT_LEDS_QTY   4

//------------------------------------------------------------------------------
/** @brief Queue of data to be transmitted by UART.                         */
extern fifo_t output;

//------------------------------------------------------------------------------
/** @brief Semaphore to access the output queue.                            */
 extern struct pt_sem sem;

//------------------------------------------------------------------------------
/** @brief Flags for LED control.                                           */
extern bool leds[];

//------------------------------------------------------------------------------
/** @brief Initializes the data objects that need.                         */
void allDataObj_init( void );

//------------------------------------------------------------------------------
#endif
