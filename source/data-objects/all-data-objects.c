
//------------------------------------------------------------------------------
/** @file all-data-objects.c
  * @brief Defines the objects  of data visible between tasks
  *         in different modules.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------

#include "all-data-objects.h"
#include "../drivers/hardware-profile/hardware-abstraction-layer.h"

//------------------------------------------------------------------------------
/** @brief Output queue length.                                             */
#define _OUTPUT_LENGTH       96

//------------------------------------------------------------------------------
/** @brief Memory for the output queue.                                     */
uint8_t outputData[ _OUTPUT_LENGTH ];

//------------------------------------------------------------------------------
// Output queue.
fifo_t output = { 0, 0, 0, _OUTPUT_LENGTH, outputData };

//------------------------------------------------------------------------------
// Semaphore to access the output queue.
struct pt_sem sem;

//------------------------------------------------------------------------------
// Flag for LED control.                                                   
bool leds[ DATA_OBJECT_LEDS_QTY ];

//------------------------------------------------------------------------------
// Initializes the data objects that need:
void allDataObj_init( void ) { 
    uint8_t i;
    for( i = 0; i < DATA_OBJECT_LEDS_QTY; i++ )
        leds[i] = false;
    PT_SEM_INIT( &sem, 1 );
}

//------------------------------------------------------------------------------
