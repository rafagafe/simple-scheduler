
//------------------------------------------------------------------------------
/** @file fifo.c
  * @brief  Defines a structure and functions able to handle byte queues.
  *         The queue size is configurable individually.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------

#include "fifo.h"

//------------------------------------------------------------------------------
// Checks if queue is full:
bool fifo_isFull( fifo_t *fifo ) { return (fifo->quantity >= fifo->size); }

//------------------------------------------------------------------------------
// Checks if queue is empty:
bool fifo_isEmpty( fifo_t *fifo ) { return !fifo->quantity; }

//------------------------------------------------------------------------------
// Checks if queue is not full:
bool fifo_isNotFull( fifo_t *fifo ) { return (fifo->quantity < fifo->size); }

//------------------------------------------------------------------------------
// Gets the quantity of bytes of data in queue:
bool fifo_isNotEmpty( fifo_t *fifo ) { return (bool)(fifo->quantity); }

//------------------------------------------------------------------------------
// Gets the quantity of bytes of data in queue:
unsigned fifo_getDataQty( fifo_t *fifo ) { return fifo->quantity; }

//------------------------------------------------------------------------------
// Gets the size of the free space in bytes.
unsigned fifo_getSpaceAvilable( fifo_t *fifo ) {
    // Resta al espacio total el espacio utilizado:
    return (fifo->size - fifo->quantity);
}

//------------------------------------------------------------------------------
// Empty the queue.
void fifo_flush( fifo_t *fifo ) {
    fifo->first = fifo->last = fifo->quantity = 0;
}

//------------------------------------------------------------------------------
// Puts a byte into the queue.
bool fifo_put( fifo_t *fifo, uint8_t byte ) {
    // Si la cola est� llena no se puede meter nada:
    if ( fifo_isFull( fifo ) ) return false;
    // Se pone el byte detr�s del �ltimo de la cola:
    fifo->bytes[fifo->last] = byte;
    // Se incrementa el �ndice:
    if ( ++fifo->last >= fifo->size ) fifo->last = 0;
    // Se incrementa el contador de bytes:
    fifo->quantity++;
    // Devuelve true porque ha conseguido meter el dato:
    return true;
}

//------------------------------------------------------------------------------
// Gets a byte from queue.
bool fifo_get( fifo_t *fifo, uint8_t *byte ) {
    // Si la cola est� vac�a no se puede sacar nada:
    if ( fifo_isEmpty( fifo ) ) return false;
    // Se copia el pirmer byte de la cola:
    *byte = fifo->bytes[fifo->first];
    // Se incrementa el �ndice:
    if ( ++fifo->first >= fifo->size ) fifo->first = 0;
    // Se decrementa el contador de bytes:
    fifo->quantity--;
    // Devuelve true porque ha conseguido sacar el dato:
    return true;
}

//------------------------------------------------------------------------------