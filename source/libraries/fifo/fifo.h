

//------------------------------------------------------------------------------
/** @file fifo.h
  * @brief  Defines a structure and functions able to handle byte queues.
  *         The queue size is configurable individually.
  * @date 04/01/2014.
  * @author Rafa Garc�a.                                                    */
//------------------------------------------------------------------------------


#ifndef _FIFO_
#define _FIFO_
//------------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>

//------------------------------------------------------------------------------
/** @brief Structure capable of storing a byte queue.                       */
struct fifo_s {
  unsigned first;       /**< Index of the first byte of the queue.          */
  unsigned last;        /**< Index of free space after the last byte.       */
  unsigned quantity;    /**< Quantity of bytes in the queue.                */
  const unsigned size;  /**< Must be initialized to the size of the array.  */
  uint8_t *const bytes; /**< Points to the array of bytes                   */
};

//------------------------------------------------------------------------------
/** @brief Type queue of bytes.                                             */
typedef struct fifo_s fifo_t;

//------------------------------------------------------------------------------
/** @brief Checks if queue is full.
  * @param fifo: Reference to queue.
  * @return True if full, false in otherwise.                               */
bool fifo_isFull( fifo_t *fifo );

//------------------------------------------------------------------------------
/** @brief Checks if queue is empty.
  * @param fifo: Reference to queue.
  * @return True if empty, false in otherwise.                              */
bool fifo_isEmpty( fifo_t *fifo );

//------------------------------------------------------------------------------
/** @brief Checks if queue is not full.
  * @param fifo: Reference to queue.
  * @return True if not full, false in otherwise.                           */
bool fifo_isNotFull( fifo_t *fifo );

//------------------------------------------------------------------------------
/** @brief Checks if queue is not empty.
  * @param fifo: Reference to queue.
  * @return True if not empty, false in otherwise.                          */
bool fifo_isNotEmpty( fifo_t *fifo );

//------------------------------------------------------------------------------
/** @brief Gets the quantity of bytes of data in queue.
  * @param fifo: Reference to queue.
  * @return Quantity of bytes.                                              */
unsigned fifo_getDataQty( fifo_t *fifo );

//------------------------------------------------------------------------------
/** @brief Gets the size of the free space in bytes.
  * @param fifo: Reference to queue.
  * @return Bytes quantity.                                                 */
unsigned fifo_getSpaceAvilable( fifo_t *fifo );

//------------------------------------------------------------------------------
/** @brief Empty the queue.
  * @param fifo: Reference to queue.                                        */
void fifo_flush( fifo_t *fifo );

//------------------------------------------------------------------------------
/** @brief Puts a byte into the queue.
  * @param fifo: Reference to queue.
  * @param byte: Paquete fuente.
  * @return True si lo a metido, flase en otro caso.                         */
bool fifo_put( fifo_t *fifo, uint8_t byte );

//------------------------------------------------------------------------------
/** @brief Gets a byte from queue.
  * @param fifo: Reference to queue.
  * @param frame: Byte destino.
  * @return True si lo a sacado, flase en otro caso.                        */
bool fifo_get( fifo_t *fifo, uint8_t *byte );

//------------------------------------------------------------------------------
#endif