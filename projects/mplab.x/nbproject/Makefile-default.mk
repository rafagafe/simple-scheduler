#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/mplab.x.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/mplab.x.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../../source/data-objects/all-data-objects.c ../../source/drivers/hardware-profile/hardware-abstraction-layer.c ../../source/drivers/timer/timer.c ../../source/entry/main.c ../../source/libraries/fifo/fifo.c ../../source/scheduler/scheduler.c ../../source/tasks/leds-switches/leds-switches-tasks.c ../../source/tasks/periodic-end/periodic-end-tasks.c ../../source/tasks/timed-text/timed-text-tasks.c ../../source/tasks/uart/uart-tasks.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1593835973/all-data-objects.o ${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o ${OBJECTDIR}/_ext/1846320173/timer.o ${OBJECTDIR}/_ext/793036002/main.o ${OBJECTDIR}/_ext/1172219562/fifo.o ${OBJECTDIR}/_ext/1332919655/scheduler.o ${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o ${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o ${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o ${OBJECTDIR}/_ext/880418947/uart-tasks.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1593835973/all-data-objects.o.d ${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o.d ${OBJECTDIR}/_ext/1846320173/timer.o.d ${OBJECTDIR}/_ext/793036002/main.o.d ${OBJECTDIR}/_ext/1172219562/fifo.o.d ${OBJECTDIR}/_ext/1332919655/scheduler.o.d ${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o.d ${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o.d ${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o.d ${OBJECTDIR}/_ext/880418947/uart-tasks.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1593835973/all-data-objects.o ${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o ${OBJECTDIR}/_ext/1846320173/timer.o ${OBJECTDIR}/_ext/793036002/main.o ${OBJECTDIR}/_ext/1172219562/fifo.o ${OBJECTDIR}/_ext/1332919655/scheduler.o ${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o ${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o ${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o ${OBJECTDIR}/_ext/880418947/uart-tasks.o

# Source Files
SOURCEFILES=../../source/data-objects/all-data-objects.c ../../source/drivers/hardware-profile/hardware-abstraction-layer.c ../../source/drivers/timer/timer.c ../../source/entry/main.c ../../source/libraries/fifo/fifo.c ../../source/scheduler/scheduler.c ../../source/tasks/leds-switches/leds-switches-tasks.c ../../source/tasks/periodic-end/periodic-end-tasks.c ../../source/tasks/timed-text/timed-text-tasks.c ../../source/tasks/uart/uart-tasks.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
	${MAKE} ${MAKE_OPTIONS} -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/mplab.x.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=24FJ128GA010
MP_LINKER_FILE_OPTION=,--script=p24FJ128GA010.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1593835973/all-data-objects.o: ../../source/data-objects/all-data-objects.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1593835973 
	@${RM} ${OBJECTDIR}/_ext/1593835973/all-data-objects.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/data-objects/all-data-objects.c  -o ${OBJECTDIR}/_ext/1593835973/all-data-objects.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1593835973/all-data-objects.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1593835973/all-data-objects.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o: ../../source/drivers/hardware-profile/hardware-abstraction-layer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1334555868 
	@${RM} ${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/drivers/hardware-profile/hardware-abstraction-layer.c  -o ${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1846320173/timer.o: ../../source/drivers/timer/timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1846320173 
	@${RM} ${OBJECTDIR}/_ext/1846320173/timer.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/drivers/timer/timer.c  -o ${OBJECTDIR}/_ext/1846320173/timer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1846320173/timer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1846320173/timer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/793036002/main.o: ../../source/entry/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/793036002 
	@${RM} ${OBJECTDIR}/_ext/793036002/main.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/entry/main.c  -o ${OBJECTDIR}/_ext/793036002/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/793036002/main.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/793036002/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1172219562/fifo.o: ../../source/libraries/fifo/fifo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1172219562 
	@${RM} ${OBJECTDIR}/_ext/1172219562/fifo.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/libraries/fifo/fifo.c  -o ${OBJECTDIR}/_ext/1172219562/fifo.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1172219562/fifo.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1172219562/fifo.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1332919655/scheduler.o: ../../source/scheduler/scheduler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1332919655 
	@${RM} ${OBJECTDIR}/_ext/1332919655/scheduler.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/scheduler/scheduler.c  -o ${OBJECTDIR}/_ext/1332919655/scheduler.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1332919655/scheduler.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1332919655/scheduler.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o: ../../source/tasks/leds-switches/leds-switches-tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/315699250 
	@${RM} ${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/tasks/leds-switches/leds-switches-tasks.c  -o ${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o: ../../source/tasks/periodic-end/periodic-end-tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1026539426 
	@${RM} ${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/tasks/periodic-end/periodic-end-tasks.c  -o ${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o: ../../source/tasks/timed-text/timed-text-tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2072107784 
	@${RM} ${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/tasks/timed-text/timed-text-tasks.c  -o ${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/880418947/uart-tasks.o: ../../source/tasks/uart/uart-tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/880418947 
	@${RM} ${OBJECTDIR}/_ext/880418947/uart-tasks.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/tasks/uart/uart-tasks.c  -o ${OBJECTDIR}/_ext/880418947/uart-tasks.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/880418947/uart-tasks.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/880418947/uart-tasks.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/_ext/1593835973/all-data-objects.o: ../../source/data-objects/all-data-objects.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1593835973 
	@${RM} ${OBJECTDIR}/_ext/1593835973/all-data-objects.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/data-objects/all-data-objects.c  -o ${OBJECTDIR}/_ext/1593835973/all-data-objects.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1593835973/all-data-objects.o.d"      -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1593835973/all-data-objects.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o: ../../source/drivers/hardware-profile/hardware-abstraction-layer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1334555868 
	@${RM} ${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/drivers/hardware-profile/hardware-abstraction-layer.c  -o ${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o.d"      -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1334555868/hardware-abstraction-layer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1846320173/timer.o: ../../source/drivers/timer/timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1846320173 
	@${RM} ${OBJECTDIR}/_ext/1846320173/timer.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/drivers/timer/timer.c  -o ${OBJECTDIR}/_ext/1846320173/timer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1846320173/timer.o.d"      -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1846320173/timer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/793036002/main.o: ../../source/entry/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/793036002 
	@${RM} ${OBJECTDIR}/_ext/793036002/main.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/entry/main.c  -o ${OBJECTDIR}/_ext/793036002/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/793036002/main.o.d"      -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/793036002/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1172219562/fifo.o: ../../source/libraries/fifo/fifo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1172219562 
	@${RM} ${OBJECTDIR}/_ext/1172219562/fifo.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/libraries/fifo/fifo.c  -o ${OBJECTDIR}/_ext/1172219562/fifo.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1172219562/fifo.o.d"      -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1172219562/fifo.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1332919655/scheduler.o: ../../source/scheduler/scheduler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1332919655 
	@${RM} ${OBJECTDIR}/_ext/1332919655/scheduler.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/scheduler/scheduler.c  -o ${OBJECTDIR}/_ext/1332919655/scheduler.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1332919655/scheduler.o.d"      -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1332919655/scheduler.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o: ../../source/tasks/leds-switches/leds-switches-tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/315699250 
	@${RM} ${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/tasks/leds-switches/leds-switches-tasks.c  -o ${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o.d"      -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/315699250/leds-switches-tasks.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o: ../../source/tasks/periodic-end/periodic-end-tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1026539426 
	@${RM} ${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/tasks/periodic-end/periodic-end-tasks.c  -o ${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o.d"      -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/1026539426/periodic-end-tasks.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o: ../../source/tasks/timed-text/timed-text-tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2072107784 
	@${RM} ${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/tasks/timed-text/timed-text-tasks.c  -o ${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o.d"      -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/2072107784/timed-text-tasks.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/880418947/uart-tasks.o: ../../source/tasks/uart/uart-tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/880418947 
	@${RM} ${OBJECTDIR}/_ext/880418947/uart-tasks.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../source/tasks/uart/uart-tasks.c  -o ${OBJECTDIR}/_ext/880418947/uart-tasks.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/880418947/uart-tasks.o.d"      -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/_ext/880418947/uart-tasks.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/mplab.x.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/mplab.x.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -Wl,--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,$(MP_LINKER_FILE_OPTION),--heap=384,--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/mplab.x.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/mplab.x.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -Wl,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--heap=384,--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}\\xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/mplab.x.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf 
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
